module.exports = function(client, params) {
  const data = [params.player, params.team, params.total_value, params.total_guaranteed, params.average, params.yearly_guaranteed, params.percent_guaranteed, params.free_agency_year, params.free_agency_type, params.position]

  const query = `INSERT INTO players (
    player, team, total_value, total_guaranteed, average, yearly_guaranteed, percent_guaranteed, free_agency_year, free_agency_type, position
  )

  VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8, $9, $10
  );`

  client.query(query, data, () => console.log(`Successfully added ${params.player}`))
}
