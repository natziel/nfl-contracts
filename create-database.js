const pg = require('pg')

pg.defaults.ssl = true;

const client = new pg.Client({
  user: process.env.PGUSER, //env var: PGUSER
  database: process.env.PGDATABASE, //env var: PGDATABASE
  password: process.env.PGPASSWORD, //env var: PGPASSWORD
  host: process.env.PGPASSWORD, // Server hosting the postgres database
  port: process.env.PGPORT, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
})

client.connect((err) => {
  if (err) console.error(err)

  const query = `
  BEGIN;

  CREATE TABLE players (
    player CHAR(50) NOT NULL,
    team CHAR(50) NOT NULL,
    total_value INT NOT NULL,
    total_guaranteed INT NOT NULL,
    average INT NOT NULL,
    yearly_guaranteed INT NOT NULL,
    percent_guaranteed INT NOT NULL,
    free_agency_year INT NOT NULL,
    free_agency_type CHAR(50) NOT NULL,
    position CHAR(50) NOT NULL,
    id BIGSERIAL PRIMARY KEY
  );

  CREATE TABLE inflation_rates (
    year INT NOT NULL,
    rate INT NOT NULL
  );

  CREATE OR REPLACE FUNCTION get_years(total_value integer, average integer)
    RETURNS integer AS $$
    BEGIN
      RETURN round(total_value / average);
    END; $$
    LANGUAGE plpgsql;

  CREATE OR REPLACE FUNCTION get_year_signed(total_value integer, average integer, free_agency_year integer)
    RETURNS integer AS $$
    BEGIN
      RETURN free_agency_year - round(total_value / average);
    END; $$
    LANGUAGE plpgsql;

    CREATE OR REPLACE FUNCTION get_inflated_salary(total_value integer, average integer, free_agency_year integer)
      RETURNS integer AS $$
      DECLARE rate_v double precision;
      BEGIN
        SELECT rate into rate_v FROM inflation_rates WHERE year=(get_year_signed(total_value, average, free_agency_year));
        return round(average / (rate_v / 100) * 1.34);
      END; $$
      LANGUAGE plpgsql;
  COMMIT;`

  client
    .query(query, () => client.end())
})
