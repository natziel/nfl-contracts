const rp = require('request-promise')
const cheerio = require('cheerio')

const query = require('./insert-player')

const pg = require('pg')

pg.defaults.ssl = true;

const client = new pg.Client({
  user: process.env.PGUSER, //env var: PGUSER
  database: process.env.PGDATABASE, //env var: PGDATABASE
  password: process.env.PGPASSWORD, //env var: PGPASSWORD
  host: process.env.PGPASSWORD, // Server hosting the postgres database
  port: process.env.PGPORT, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
})

client.connect((err) => {
  if (err) console.error(err)

  scrape('quarterback')
    .then(data => data.forEach((row) => query(client, row)))
})

function group_by(arr, n) {
  return arr
    .reduce(([acc, cur], element, i) => i % n === (n - 1) ? [acc.concat([cur.concat(element)]), []] : [acc, cur.concat(element)], [[], []])[0]
}

function de_tuple(tuple, keys) {
  return tuple
    .reduce((acc, element, i) => {
      acc[keys[i]] = element
      return acc
    }, {})
}

function money_to_int(money) {
  return parseInt(money.match(/[0-9]/g).join(''), 10)
}

function percent_to_int(percent) {
  return parseInt(percent.match(/[0-9]/g).join(''), 10)
}

function post_process(row) {
  row['free_agency_year'] = parseInt(row['free_agency'].split(' ')[0], 10)
  row['free_agency_type'] = row['free_agency'].split(' ')[1]

  row['total_value'] = money_to_int(row['total_value'])
  row['average'] = money_to_int(row['average'])
  row['total_guaranteed'] = money_to_int(row['total_guaranteed'])
  row['yearly_guaranteed'] = money_to_int(row['yearly_guaranteed'])

  row['percent_guaranteed'] = percent_to_int(row['percent_guaranteed'])
  return row
}

function scrape(position) {
  return rp
    .get(`http://overthecap.com/position/${position}/`)
    .then(cheerio.load)
    .then($ => $('table.position-table tbody'))
    .then($ => $.find('tr'))
    .then($ => $.map((i, cell) => cell.children))
    .then($ => $.map((i, cell) => cell.children[0]))
    .then($ => $.map((i, cell) => cell.data ? cell.data : cell.children[0].data))
    .then($ => [].slice.apply($))
    .then(data => group_by(data, 8))
    .then(data => data.map((row) => row.concat(position)))
    .then(data => data.map(row => de_tuple(row, ['player', 'team', 'total_value', 'average', 'total_guaranteed', 'yearly_guaranteed', 'percent_guaranteed', 'free_agency', 'position'])))
    .then(data => data.map(post_process))
}
