const pg = require('pg')

pg.defaults.ssl = true;

const client = new pg.Client({
  user: process.env.PGUSER, //env var: PGUSER
  database: process.env.PGDATABASE, //env var: PGDATABASE
  password: process.env.PGPASSWORD, //env var: PGPASSWORD
  host: process.env.PGPASSWORD, // Server hosting the postgres database
  port: process.env.PGPORT, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
})

client.connect((err) => {
  if (err) console.error(err)

  const query = `
  BEGIN;

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2008, 100);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2009, 106);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2010, 104);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2011, 103);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2012, 104);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2013, 106);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2014, 115);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2015, 124);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2016, 134);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2017, 142);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2018, 150);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2019, 159);

  INSERT INTO inflation_rates (
    year, rate
  ) VALUES (2020, 169);

  COMMIT;`

  client
    .query(query, () => client.end())
})
